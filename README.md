# Mappr Front End Task - 3

A Reactjs app tracking the buzz about **The Avangers** on **Twitter**.

## Project

We would like you to build a React application that provides possibilities to display Realtime Tweets. The app users should be able to see a webpage displaying the most recent Tweets about **The Avangers** Movie.

- The app should subscribe to the Realtime Twitter API to track words like *The Avangers*, *Avangers 4* and *Avangers Endgame* (try out different combinations to match the best results)
- On first load the app should display the most recent Tweets matching the mentioned filter
- Each Tweet should include the *Author image, Author name and username, Tweet text and time*
- The app should get the realtime updates from the API and display a button on top of the Tweets list saying *"X new Tweets available"*, where *X* stands for the number of new Tweets
- Clicking on the *New Tweets* button should load the new Tweets on top of the existing list on the page

This project is pretty open ended in order to leave you some freedom to improve upon the base by focusing on what you like the most.

We strongly encourage you to pick a couple of optional improvements. The following are just some ideas, so if none of these interest you, feel free to do something that isn�t on this list:

- The Tweets should have links to the real Tweets on Tweeter
- It can be possible for the User to upadte the tracked strings from the app to get better results
- The app should include Tests

## Tech stack

We are expecting this app to be developed with ReactJs preferably being based on Create React App. If you prefer a project structure other than CRA, you are free to express your style.

### Evaluation

The developer should have a separate repository (probably on GitHub), where the task would be commited during the implementaion.
When the task is done, the link to the repository should be shared with the contact person from our company.

The evaluator should be able to access the repositry and clone the final code. 
From the cloned folder the evaluator should be able to run `npm start` and see the running application to display a page with the Realtime Tweets data.
We will evaluate the exercise by looking at the end result and the code.

### Notes

Please, don't open a PR against this repo. Just follow the direction from the recruiter on how to submit the exercise.

### Coding at Mappr

At [Mappr](https://www.mappr.fr) we strive for writing simple, maintainable and clean code.

We prefer simplicity over complexity.

We comment our code and commit often.

We love our users and we really care about providing a good user experience and pleasant UI.

We encourage out of the box thinking and we love to be impressed!